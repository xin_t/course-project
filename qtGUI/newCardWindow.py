import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import newCard
from server_side import Address, Wallet

class AddNewCard(QMainWindow, newCard.Ui_Form):
    def __init__(self, email):
        super(AddNewCard, self).__init__()
        self.setupUi(self)
        self.email = email
        # show related address
        self.AddressDisplay()
        self.addButton.clicked.connect(self.AddNow)

    def AddressDisplay(self):
        address_list = Address.show_address(self.email)
        addr_list = []
        for i in range(len(address_list)):
            info = address_list[i]
            text_display = '''%s, %s, %s ;''' % (info[0], info[1], info[2])
            addr_list.append(text_display)
        slm = QStringListModel()
        slm.setStringList(addr_list)
        self.addresslistView.setModel(slm)


    def AddNow(self):
        cardnumber = self.cardNumberEdit.text()
        address_list = Address.show_address(self.email)
        select = self.addresslistView.currentIndex().row()
        if select >= 0:
            address_info = address_list[select]
            user = Wallet.CreditCard(self.email)
            user.newCard(cardnumber, address_info)
            QMessageBox.information(self, 'Success', 'Card Added')
            self.close()
        else:
            QMessageBox.warning(self, 'Warning', 'Please select billing address')







if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = AddNewCard('test')
    window.show()
    sys.exit(app.exec_())