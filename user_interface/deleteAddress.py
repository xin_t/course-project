# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'deleteAddress.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DeleteAddressUI(object):
    def setupUi(self, DeleteAddressUI):
        DeleteAddressUI.setObjectName("DeleteAddressUI")
        DeleteAddressUI.resize(523, 651)
        self.widget = QtWidgets.QWidget(DeleteAddressUI)
        self.widget.setGeometry(QtCore.QRect(30, 20, 471, 611))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(self.widget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.listWidget.setFont(font)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget)
        self.deleteaddressButton = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(14)
        self.deleteaddressButton.setFont(font)
        self.deleteaddressButton.setObjectName("deleteaddressButton")
        self.verticalLayout.addWidget(self.deleteaddressButton)

        self.retranslateUi(DeleteAddressUI)
        QtCore.QMetaObject.connectSlotsByName(DeleteAddressUI)

    def retranslateUi(self, DeleteAddressUI):
        _translate = QtCore.QCoreApplication.translate
        DeleteAddressUI.setWindowTitle(_translate("DeleteAddressUI", "Form"))
        self.label.setText(_translate("DeleteAddressUI", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">Select &amp; Delete</span></p></body></html>"))
        self.deleteaddressButton.setText(_translate("DeleteAddressUI", "Delete"))

