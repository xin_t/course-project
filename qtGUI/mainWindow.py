import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import main  # main page ui
import RegisterWindow
from server_side import SignIn, Address, Update, Wallet, Flight
import newAddressWindow
import newCardWindow
import bookingWindow
import connectionwindow


class mainWindow(QMainWindow, main.Ui_Home):
    def __init__(self):

        super(mainWindow, self).__init__()
        self.setupUi(self)
        # initialize login status
        self.emailText.setText("Please login first")
        self.passwordUpdate.setEnabled(False)
        self.iataEdit.setEnabled(False)
        self.iataUpdate.setEnabled(False)
        # address button
        self.addAddressButton.setEnabled(False)
        self.delAddressButton.setEnabled(False)
        self.refreshButton.setEnabled(False)
        # credit card button
        self.addccardButton.setEnabled(False)
        self.delccardButton.setEnabled(False)
        self.refreshccardButton.setEnabled(False)
        # connect button
        self.signUpButton.clicked.connect(self.signUp)
        self.loginButton.clicked.connect(self.login)
        # info update
        self.passwordUpdate.clicked.connect(self.update_password)
        self.iataUpdate.clicked.connect(self.update_iata)
        # address
        self.refreshButton.clicked.connect(self.address_display)
        self.addAddressButton.clicked.connect(self.new_address)
        self.delAddressButton.clicked.connect(self.del_address)
        # credit card
        self.refreshccardButton.clicked.connect(self.credit_card_display)
        self.delccardButton.clicked.connect(self.del_credit_card)
        self.addccardButton.clicked.connect(self.new_card)
        # Mileage
        self.RefreashPoint.setEnabled(False)
        self.RefreashPoint.clicked.connect(self.Mileage)
        # flight section
        self.bookFlightButton.setEnabled(False)
        # vaild iata
        self.iatalist = Flight.Flight().ShowAirport()
        self.fromcomboBox.addItems(self.iatalist)
        self.tocomboBox.addItems(self.iatalist)
        # click search button
        self.departureFlightButton.clicked.connect(self.show_flight_info)
        self.returnFlightButton.clicked.connect(self.showReturnflight)
        self.flightinfo = []
        self.bookFlightButton.clicked.connect(self.booking)
        # search connection
        self.classcomboBox.addItems(['First', 'Economy'])
        self.maxprice.setText('600')
        self.connectionButton.clicked.connect(self.connectionFlight)
        # ManageBooking
        self.refreshBooking.setEnabled(False)
        self.DeleteBookinng.setEnabled(False)
        self.refreshBooking.clicked.connect(self.showBooking)
        self.DeleteBookinng.clicked.connect(self.delBookinng)
        self.connectionButton.setEnabled(False)
        self.connectionButton.clicked.connect(self.connectionFlight)


    def signUp(self):
        self.register = RegisterWindow.Register()
        self.register.show()

    def login(self):
        # read input text (String)
        email = self.emailText.text()
        password = self.passwordEdit.text()
        if self.logStatus.text() == "False":
            result = SignIn.SignIn(email, password)
            if result == False:
                print(result)
                QMessageBox.warning(self, "Warning", "Wrong User Info, please try again！")
            else:
                # login success, pass user info
                print('cool login')
                QMessageBox.information(self, "Success", "Login Success")
                # update login status
                log_email = result[0][0]
                iata = result[0][4]
                self.logStatus.setText(log_email)
                self.loginButton.setEnabled(False)
                self.signUpButton.setEnabled(False)
                self.bookFlightButton.setEnabled(True)
                # update display info
                self.emailText.setText(log_email)
                self.emailText.setEnabled(False)
                self.iataEdit.setEnabled(True)
                self.iataEdit.setText(iata)
                self.passwordUpdate.setEnabled(True)
                self.iataUpdate.setEnabled(True)
                # address
                self.refreshButton.setEnabled(True)
                # card button
                self.refreshccardButton.setEnabled(True)
                self.RefreashPoint.setEnabled(True)
                # booking button
                self.refreshBooking.setEnabled(True)
                self.connectionButton.setEnabled(True)


    def Mileage(self):
        account = self.logStatus.text()
        point = Wallet.CheckPoint(account)[0][0]
        self.pointNumber.display(int(point))


    def update_password(self):
        user = Update.UpdateUser(self.logStatus.text())
        password = self.passwordEdit.text()
        user.password(password)
        QMessageBox.information(self, "Success", "Password has been changed")

    def update_iata(self):
        up_user = Update.UpdateUser(self.logStatus.text())
        iata = self.iataEdit.text()
        iata_valid = up_user.iata(iata)
        if iata_valid:
            QMessageBox.information(self, "Success", "Home airport has been changed")
        else:
            QMessageBox.warning(self, "Warning", "Invalid iata")

    # address function
    def address_display(self):
        address_list = Address.show_address(self.logStatus.text())
        addr_list = []
        for i in range(len(address_list)):
            info = address_list[i]
            text_display = '''%s, %s, %s ;''' % (info[0], info[1], info[2])
            addr_list.append(text_display)
        slm = QStringListModel()
        slm.setStringList(addr_list)
        self.adressListView.setModel(slm)
        self.addAddressButton.setEnabled(True)
        self.delAddressButton.setEnabled(True)

    def del_address(self):
        address_list = Address.show_address(self.logStatus.text())
        select = self.adressListView.currentIndex().row()
        del_info = address_list[select]
        update = Address.update_address(del_info[0], del_info[1], del_info[2], del_info[3])
        if update.check_card_use():
            update.delete()
            QMessageBox.information(self, "Success", "Please refresh before new deletion")
            self.delAddressButton.setEnabled(False)
        else:
            QMessageBox.warning(self, "Warning", "Address in use, please delete card first")


    def new_address(self):
        self.add_address = newAddressWindow.newAddress(self.logStatus.text())
        self.add_address.show()

    # credit card function
    def credit_card_display(self):
        credit_card_list = Wallet.CreditCard(self.logStatus.text()).showCard()
        card_list = []
        for i in range(len(credit_card_list)):
            info = credit_card_list[i]
            text_display = '''%s || %s, %s, %s ;''' % (info[0], info[2], info[3], info[4])
            card_list.append(text_display)
        slm = QStringListModel()
        slm.setStringList(card_list)
        self.ccardListview.setModel(slm)
        self.addccardButton.setEnabled(True)
        self.delccardButton.setEnabled(True)

    def del_credit_card(self):
        credit_card_list = Wallet.CreditCard(self.logStatus.text()).showCard()
        select = self.ccardListview.currentIndex().row()
        del_info = credit_card_list[select]
        user_card = Wallet.CreditCard(self.logStatus.text())
        user_card.delCard(del_info[0])
        self.delccardButton.setEnabled(False)
        QMessageBox.information(self, 'Success', 'Please refresh before next deletion')

    def new_card(self):
        self.new_card_window = newCardWindow.AddNewCard(self.logStatus.text())
        self.new_card_window.show()

    def showBooking(self):
        account = self.logStatus.text()
        booking_list = Flight.ManageBooking(account).showBooking()
        show_list = []
        for b in booking_list:
            record = 'Flight: %s%s On: %s Class: %s Price: $%s'%(b[0],str(b[1]),b[2],b[3], str(b[4]))
            show_list.append(record)
        print(show_list)
        slm = QStringListModel()
        slm.setStringList(show_list)
        self.mblistView.setModel(slm)
        self.DeleteBookinng.setEnabled(True)

    def delBookinng(self):
        account = self.logStatus.text()
        bookinglist = Flight.ManageBooking(account).showBooking()
        selection = self.mblistView.currentIndex().row()
        del_info = bookinglist[selection]
        Flight.ManageBooking(account).delBooking(del_info[0],del_info[1],del_info[2],del_info[3])
        QMessageBox.information(self, "Success", "Please refresh before new deletion")
        self.DeleteBookinng.setEnabled(False)

    def show_flight_info(self):
        self.flightinfo = []
        departure_iata = self.fromcomboBox.currentText()
        arrival_iata = self.tocomboBox.currentText()
        year = self.leavedate.date().year()
        year = '%d'%year
        month = self.leavedate.date().month()
        if month < 10:
            month = '0%d'%month
        else:
            month = '%d'%month
        day = self.leavedate.date().day()
        if day < 10:
            day = '0%d'%day
        else:
            day = '%d'%day
        date = year + month + day
        direct_flight = Flight.DirectFlight(departure_iata, date, arrival_iata).match()
        self.flightinfo += direct_flight
        flight_info = []
        for l in direct_flight:
            str = '''Flight %s%s take off from %s at %s, arrive %s at %s on %s''' %(l[0],l[1],departure_iata, l[3],arrival_iata, l[5], date)
            flight_info.append(str)
        print(flight_info)
        slm = QStringListModel()
        slm.setStringList(flight_info)
        self.flightInfolistView.setModel(slm)
        print(self.flightinfo)


    def showReturnflight(self):
        self.flightinfo = []
        departure_iata = self.tocomboBox.currentText()
        arrival_iata = self.fromcomboBox.currentText()
        year = self.returndate.date().year()
        year = '%d' % year
        month = self.returndate.date().month()
        if month < 10:
            month = '0%d' % month
        else:
            month = '%d' % month
        day = self.returndate.date().day()
        if day < 10:
            day = '0%d' % day
        else:
            day = '%d' % day
        date = year + month + day
        direct_flight = Flight.DirectFlight(departure_iata, date, arrival_iata).match()
        self.flightinfo += direct_flight
        flight_info = []
        for l in direct_flight:
            str = '''Flight %s%s take off from %s at %s, arrive %s at %s on %s''' % (
            l[0], l[1], departure_iata, l[3], arrival_iata, l[5], date)
            flight_info.append(str)
        print(flight_info)
        slm = QStringListModel()
        slm.setStringList(flight_info)
        self.flightInfolistView.setModel(slm)

    def booking(self):
        email = self.logStatus.text()
        selection = self.flightInfolistView.currentIndex().row()
        info = self.flightinfo[selection]
        self.bookwindow = bookingWindow.Booking(email,info)
        self.bookwindow.show()

    def connectionFlight(self):
        info = []
        email = self.logStatus.text()
        takeof = self.fromcomboBox.currentText()
        land = self.tocomboBox.currentText()
        year = self.leavedate.date().year()
        year = '%d' % year
        month = self.leavedate.date().month()
        if month < 10:
            month = '0%d' % month
        else:
            month = '%d' % month
        day = self.leavedate.date().day()
        if day < 10:
            day = '0%d' % day
        else:
            day = '%d' % day
        date = year + month + day
        connection = self.spinBox.value()
        maxlength = self.spinBox_2.value()
        price = self.maxprice.text()
        seat = self.classcomboBox.currentText()
        info.append(email)
        info.append(takeof)
        info.append(land)
        info.append(date)
        info.append(connection)
        info.append(maxlength)
        info.append(int(price))
        info.append(seat)
        if connection == 1:
            path = Flight.Connection(info).PossiblePath()
            path_flight = Flight.Connection(info).flightCombo(path)
            self.cbook = connectionwindow.Connection(path_flight, email, date, seat)
            self.cbook.show()
















if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = mainWindow()
    window.show()
    sys.exit(app.exec_())

