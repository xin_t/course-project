import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import Register
from server_side import SignUp

class Register(QMainWindow, Register.Ui_MainWindow):
    def __init__(self):
        super(Register, self).__init__()
        self.setupUi(self)
        self.RegisterButton.clicked.connect(self.register)

    def register(self):
        email = self.emailEdit.text()
        password = self.passwordEdit.text()
        firstname = self.firstnameEdit.text()
        middlename = self.middleNameEdit.text()
        lastname = self.lastnameEdit.text()
        iata = self.iataEdit.text()
        result = SignUp.SignUp(email, password,firstname,middlename,lastname,iata)
        if result == 0:
            # success
            QMessageBox.information(self, "Success", "You can login now !")
            self.close()
        elif result == 1:
            # taken email
            QMessageBox.information(self, "Invalid email", "Please change email")
        elif result == 2:
            # invalid iata
            QMessageBox.information(self,"Invalid IATA", "Invalid IATA, please try again")


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Register()
    window.show()
    sys.exit(app.exec_())