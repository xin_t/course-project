INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('test', 'Jack', 'B', 'Tom', 'NYC', '123');
INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('hello@email.com', 'T', 'O', 'M', 'NYC', '123');
INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('hi@email.com', 'T', 'O', 'M', 'NYC', '123456');
INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('abc@email.com', 'T', 'O', 'M', 'NYC', '1234');
INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('abcd@email.com', 'a', 'b', 'c', 'NYC', '1234');
INSERT INTO public.customer (customeremail, firstname, middlename, lastname, iata, password) VALUES ('qwe@email.com', 'A', 'B', 'C', 'NYC', '1234');