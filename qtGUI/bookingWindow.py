import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from server_side import Flight, Wallet, Database, Book
import random


import booking

class Booking(QMainWindow, booking.Ui_booking):
    def __init__(self, email, info):
        super(Booking, self).__init__()
        self.setupUi(self)
        self.email = email
        self.info = info
        print(info)
        self.getInfo()
        self.payment()
        self.bookButton.clicked.connect(self.bookflight)


    def getInfo(self):
        airlinecode = self.info[0]
        flightnumber = str(self.info[1])
        fligth = airlinecode + flightnumber
        date = self.info[2]
        departurefrom = Flight.Flight().ShowFlight(airlinecode, flightnumber, date)[0][0]
        departuretime = self.info[3]
        destination = self.info[4]
        destinationtime = self.info[5]
        self.fromLabel.setText(departurefrom)
        self.fromTime.setText(departuretime)
        self.toLabel.setText(destination)
        self.totime.setText(destinationtime)
        self.dateLabel.setText(date)
        self.flightLabel.setText(fligth)
        price = Flight.Flight().ShowPrice(flightnumber, date)
        print(price)
        self.pricecomboBox.addItems(price)

    def payment(self):
        credit_card_list = Wallet.CreditCard(self.email).showCard()
        card_list = []
        for i in range(len(credit_card_list)):
            info = credit_card_list[i]
            text_display = '''%s''' % (info[0])
            card_list.append(text_display)
        # slm = QStringListModel()
        # slm.setStringList(card_list)
        # self.listView.setModel(slm)
        self.cardcomboBox.addItems(card_list)
        # create booking

    def bookflight(self):
        try:
            date = self.dateLabel.text()
            cardnumber = self.cardcomboBox.currentText()
            airlinecode = self.flightLabel.text()[:2]
            flightnumber = self.flightLabel.text()[2:]
            id = self.email + date + str(flightnumber)
            c_class = self.pricecomboBox.currentText()[:7]
            price = self.pricecomboBox.currentText()[9:]
            Book.CreateOrder(self.email, id, cardnumber, airlinecode, flightnumber, date, c_class, price)
            QMessageBox.information(self, 'Success', 'Order has been placed')
            self.close()
        except:
            QMessageBox.warning(self, 'Failed', 'can not book twice')
            self.close()


















if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Booking()
    window.show()
    sys.exit(app.exec_())