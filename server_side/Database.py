import psycopg2

# connect to data server
# connect = psycopg2.connect(database='Airline',
#                            user='postgres',
#                            password='123',
#                            port='5432')
# cursor = connect.cursor()

# cursor.execute(sql)
# cursor.execute("""commit;""")
# records=cursor.fetchall()
# disconnect database


class Data(object):
    # connect to database
    def __init__(self):
        self.selected_database = psycopg2.connect(database='Airline',
                               user='postgres',
                               password='123',
                               port='5432')

    def connect(self):
        return self.selected_database

    # check if iata is valid
    def check_iata(self, iata):
        # Check if iata is valid
        cursor = self.selected_database.cursor()
        iata_set = set([])
        iata_sql = 'SELECT iata FROM airport;'
        cursor.execute(iata_sql)
        iata_list = cursor.fetchall()
        cursor.close()
        self.selected_database.close()
        for a in range(len(iata_list)):
            iata_set.add(iata_list[a][0])
        if iata in iata_set:
            return True
        else:
            return False
