import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import RegisterWindow

import HomePage


class HomePage(QMainWindow, HomePage.Ui_MainWindow):

    def __init__(self):
        super(HomePage, self).__init__()
        self.setupUi(self)

        self.UpdateInfoButoon.clicked.connect(self.update)

    def update(self):
        registerUI.show()




if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = HomePage()
    registerUI = RegisterWindow.Register()
    window.show()
    sys.exit(app.exec_())
