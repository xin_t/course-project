import connection
import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from server_side import Flight, Wallet, Database, Book

class Connection(QMainWindow, connection.Ui_Form):
    def __init__(self, info, email, date, seat):
        super(Connection, self).__init__()
        self.setupUi(self)
        self.email = email
        self.date = date
        self.seat = seat
        # first flight display
        self.payment()
        if len(info) == 2:
            flight1 = info[0][0]
            list1 = []
            for f in flight1:
                ac = f[0]
                fn = str(f[1])
                r = ac + fn
                list1.append(r)
            self.phase1.addItems(list1)
            flight2 = info[1][0]
            list2 = []
            for f2 in flight2:
                ac = f2[0]
                fn = str(f2[1])
                r = ac + fn
                list2.append(r)
            self.phase2.addItems(list2)
        self.Refresh()
        self.refreshe.clicked.connect(self.Refresh)
        self.confirm.setEnabled(False)
        self.confirm.clicked.connect(self.bookcombo)


    def getPrice(self):
        flight1 = self.phase1.currentText()
        price1 = Flight.Flight().ShowSeatPrice(flight1, self.date, self.seat)
        flight2 = self.phase2.currentText()
        price2 = Flight.Flight().ShowSeatPrice(flight2, self.date, self.seat)
        total = price1 + price2
        text1 = flight1 + str(price1)
        self.flight1price.setText(text1)
        text2 = flight2 + str(price2)
        self.fligt2price.setText(text2)
        return total

    def getLength(self):
        flight1 = self.phase1.currentText()
        length = Flight.Flight().ShowLength(flight1, self.date)
        flight2 = self.phase2.currentText()
        length2 = Flight.Flight().ShowLength(flight2, self.date)
        total = length + length2
        return total

    def Refresh(self):
        price = self.getPrice()
        length = self.getLength()
        self.totalprice.setText(str(price))
        self.totallength.setText(str(length))
        self.confirm.setEnabled(True)

    def payment(self):
        credit_card_list = Wallet.CreditCard(self.email).showCard()
        card_list = []
        for i in range(len(credit_card_list)):
            info = credit_card_list[i]
            text_display = '''%s''' % (info[0])
            card_list.append(text_display)
        self.cardview.addItems(card_list)
        # create booking

    def bookcombo(self):
        try:
            info1 = self.flight1price.text()
            print(info1)
            self.book(info1)
            info2 = self.fligt2price.text()
            self.book(info2)
            QMessageBox.information(self,'Success','good to go')
            self.close()
        except:
            QMessageBox.warning(self, 'Failed', 'Error')

    def book(self, info):
        try:
            date = self.date
            cardnumber = self.cardview.currentText()
            airlinecode = info[:2]
            flightnumber = info[2:6]
            id = self.email + date + str(flightnumber)
            c_class = self.seat
            price = info[6:]
            Book.CreateOrder(self.email, id, cardnumber, airlinecode, flightnumber, date, c_class, price)
        except:
            QMessageBox.warning(self, 'Failed', 'Error')







if __name__ == '__main__':
    info = [[[('DL', 3333, '20210520', '0700', 'YMQ', '0900')]], [[('AC', 2714, '20210520', '1900', 'NYC', '2100'), ('DL', 3399, '20210520', '1100', 'NYC', '1600')]]]
    app = QApplication(sys.argv)
    window = Connection(info, 'test', '20210520', 'Economy')
    window.show()

    sys.exit(app.exec_())
