from server_side.Database import Data

def SignUp(email,password, firstname, middlename, lastname, iata):
    # connect to data server
    error_code = 0  # return error: 1 for invalid email / 2 for invalid iata / 0 for success
    connect = Data().connect()
    cursor = connect.cursor()
    # check exist email
    # check exist email
    email_sql = 'SELECT customeremail FROM customer;'
    cursor.execute(email_sql)
    email_list = cursor.fetchall()
    cursor.close()
    email_set = set([])
    for i in range(len(email_list)):
        email_set.add(email_list[i][0])
    # check if input is already in set
    if email in email_set:
        print('email is taken, please change')
        error_code = 1
        return error_code
        # call back to change
    else:
        # Check if iata is valid
        iata_set = set([])
        cursor = connect.cursor()
        iata_sql = 'SELECT iata FROM airport;'
        cursor.execute(iata_sql)
        iata_list = cursor.fetchall()
        cursor.close()
        for a in range(len(iata_list)):
            iata_set.add(iata_list[a][0])
        if iata in iata_set:
            sign_up = '''INSERT INTO customer VALUES ('%s','%s','%s','%s','%s','%s');''' % (
            email, firstname, middlename, lastname, iata, password)
            cursor = connect.cursor()
            cursor.execute(sign_up)
            cursor.execute("""commit;""")
            cursor.close()
            print('Sign Up Success')
            error_code = 0
            return error_code
        else:
            error_code = 2
            print('Invalid iata')
            return error_code
    # disconnect database
    connect.close()

