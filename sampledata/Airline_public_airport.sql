INSERT INTO public.airport (iata, state, country) VALUES ('YVR', 'B.C', 'CANADA');
INSERT INTO public.airport (iata, state, country) VALUES ('NYC', 'NY', 'U.S.');
INSERT INTO public.airport (iata, state, country) VALUES ('CHI', 'ILL', 'U.S.');
INSERT INTO public.airport (iata, state, country) VALUES ('LAX', 'CAL', 'U.S.');
INSERT INTO public.airport (iata, state, country) VALUES ('YTO', 'Toronto', 'CANADA');
INSERT INTO public.airport (iata, state, country) VALUES ('YMQ', 'Montreal', 'CANADA');
INSERT INTO public.airport (iata, state, country) VALUES ('SFO', 'CAL', 'U.S.');