from server_side.Database import Data

class Flight(object):
    # connect to database
    def __init__(self):
        self.connect = Data().connect()
    # show valid airport
    def ShowAirport(self):
        cursor = self.connect.cursor()
        sql = 'SELECT iata FROM airport;'
        cursor.execute(sql)
        result = cursor.fetchall()
        cursor.close()
        self.connect.close()
        list = []
        for i in range(len(result)):
            list.append(result[i][0])
        return list

    def ShowAirline(self):
        cursor = self.connect.cursor()
        sql = 'SELECT * FROM airlinecompany;'
        cursor.execute(sql)
        result = cursor.fetchall()
        cursor.close()
        self.connect.close()
        return result

    def ShowFlight(self, airlinecode, flightnumber, date):
        cursor = self.connect.cursor()
        sql = '''SELECT iata FROM flight WHERE airlinecode = '%s' and flightnumber = '%s' and date = '%s';'''%(airlinecode, flightnumber, date)
        cursor.execute(sql)
        result = cursor.fetchall()
        cursor.close()
        self.connect.close()
        return result

    def ShowTakeoffFlight(self, takeoff, date, land):
        # get all flight from takeoff airport
        cursor = self.connect.cursor()
        sql = '''SELECT * FROM flight WHERE iata = '%s' and date = '%s';''' %(takeoff, date)
        cursor.execute(sql)
        all_takeoff_flight = cursor.fetchall()
        cursor.close()
        direct_list = []
        self.connect.close()
        return all_takeoff_flight
        # check if the plane goes to destination


    def ShowLandingFlight(self, airlinecode, flightnumber, land, date):
        connect = Data().connect()
        cursor = connect.cursor()
        sql = '''SELECT * FROM landing WHERE airlinecode = '%s' and flightnumber = '%s' and date = '%s' and iata = '%s';''' % (
            airlinecode, flightnumber, land, date)
        cursor.execute(sql)
        print(sql)
        landing_flight = cursor.fetchall()
        cursor.close()
        self.connect.close()
        return landing_flight
    def ShowSeatPrice(self, info,date, seat):
        cursor = self.connect.cursor()
        sql = '''SELECT price FROM price WHERE flightnumber = '%s' and date = '%s' and class = '%s';''' % (info[2:6], date, seat)
        cursor.execute(sql)
        result = cursor.fetchall()[0][0]
        cursor.close()
        self.connect.close()
        return result

    def ShowLength(self, flight, date):
        cursor = self.connect.cursor()
        sql = '''SELECT takeofftime, landingtime FROM landing WHERE flightnumber = '%s' and date = '%s';''' % (
        flight[2:6], date)
        cursor.execute(sql)
        result = cursor.fetchall()[0]
        cursor.close()
        self.connect.close()
        length = (int(result[1]) - int(result[0])) / 100
        return length

    def ShowPrice(self, flightnumber, date):
        cursor = self.connect.cursor()
        sql = '''SELECT class, price FROM price WHERE flightnumber = '%s' and date = '%s';'''%(flightnumber, date)
        cursor.execute(sql)
        result = cursor.fetchall()
        cursor.close()
        self.connect.close()
        list = []
        for i in result:
            string = i[0] + ' $' + str(i[1])
            list.append(string)

        if len(result) == 0:
            return False
        return list


class DirectFlight(object):
    def __init__(self, takeoff, date, land):
        self.takeoff = Flight().ShowTakeoffFlight(takeoff, date, land)
        self.date = date
        self.land = land

    def match(self):
        landing = self.ShowLandingFlight()
        match_list = []
        for t in self.takeoff:
            for l in landing:
                if  t[0] == l[0] and t[1] == l[1]:
                    match_list.append(l)
        return match_list

    def ShowLandingFlight(self):
        landing_list = []
        check_list = self.takeoff
        for c in check_list:
            airlinecode = c[0]
            flightnumber = c[1]
            connect = Data().connect()
            cursor = connect.cursor()
            sql = '''SELECT * FROM landing WHERE airlinecode = '%s' and flightnumber = '%s' and date = '%s' and iata = '%s';''' % (
                airlinecode, flightnumber, self.date, self.land)
            cursor.execute(sql)
            landing_flight = cursor.fetchall()
            cursor.close()
            connect.close()
            landing_list += landing_flight
        return landing_list



class ManageBooking(object):
    def __init__(self, email):
        self.email = email
        self.connect = Data().connect()

    def showBooking(self):
        cursor = self.connect.cursor()
        sql = '''SELECT airlinecode, flightnumber, date, class, price FROM bookflight WHERE customeremail = '%s';'''%self.email
        cursor.execute(sql)
        result = cursor.fetchall()
        cursor.close()
        self.connect.close()
        return result

    def delBooking(self, code, flight, date, seat):
        cursor = self.connect.cursor()
        sql = '''DELETE FROM bookflight WHERE customeremail = '%s' and airlinecode = '%s' and flightnumber = '%d' and date = '%s' and class = '%s';''' % (self.email, code, flight, date, seat)
        cursor.execute(sql)
        cursor.execute('''commit;''')
        cursor.close()
        self.connect.close()


class Connection(object):
    def __init__(self, info):
        self.connect = Data().connect()
        self.cursor = self.connect.cursor()
        self.email = info[0]
        self.takeoff = info[1]
        self.land = info[2]
        self.date = info[3]
        self.connection = info[4]
        self.length = info[5]
        self.price = info[6]
        self.seat = info[7]
        self.path = [self.takeoff]
        self.count = 0

    def TakeOffFlights(self, start):
        sql = '''SELECT airlinecode, flightnumber, date FROM flight WHERE iata = '%s' and date = '%s';'''%(start, self.date)
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        return result

    def Destination(self, list_flight):
        # result = []
        # sql = '''SELECT * FROM landing WHERE airlinecode = '%s' and flightnumber = '%s' and date = '%s';''' % (
        # l[0], l[1], l[2])
        # self.cursor.execute(sql)
        # add = self.cursor.fetchall()
        # result.append(add[0][4])

        result = []
        for l in list_flight:
            sql = '''SELECT * FROM landing WHERE airlinecode = '%s' and flightnumber = '%s' and date = '%s';'''%(l[0],l[1],l[2])
            self.cursor.execute(sql)
            add = self.cursor.fetchall()
            if len(add) > 0:
                result.append(add[0][4])
        return result

    def PossiblePath(self):
        if self.connection == 1:
            path = []
            start = self.TakeOffFlights(self.takeoff)
            p1 = self.Destination(start)
            for p in p1:
                if p != self.land:
                    plist = [1, 2, 3]
                    plist[0] = self.takeoff
                    p1s = self.TakeOffFlights(p)
                    p1d = self.Destination(p1s)
                    plist[1] = p
                    for p1dp in p1d:
                        p2s = self.TakeOffFlights(p1dp)
                        p2d = self.Destination(p2s)
                        plist[2] = p1dp
                        path.append(plist)

            old_list = path
            new_list = []
            for i in old_list:
                if i not in new_list:
                    new_list.append(i)
            path = new_list

            correctpath = []
            for i in path:
                if i[-1] == self.land:
                    correctpath.append(i)

            return correctpath

        if self.connection == 2:
            path = []
            start = self.TakeOffFlights(self.takeoff)
            p1 = self.Destination(start)
            for p in p1:
                plist = [1,2,3,4]
                plist[0] = self.takeoff
                p1s = self.TakeOffFlights(p)
                p1d = self.Destination(p1s)
                plist[1] = p
                for p1dp in p1d:
                    p2s = self.TakeOffFlights(p1dp)
                    p2d = self.Destination(p2s)
                    plist[2] = p1dp
                    for p2dp in p2d:
                        plist[3] = p2dp
                        path.append(plist)

            old_list = path
            new_list = []
            for i in old_list:
                if i not in new_list:
                    new_list.append(i)
            path = new_list

            correctpath = []
            for i in path:
                if i[-1] == self.land:
                    correctpath.append(i)

            return correctpath

    def flightCombo(self, list):
        combolist = []
        if self.connection == 1:
            phase1 = []
            for l in list:
                takeoff = l[0]
                land = l[1]
                date = self.date
                f = DirectFlight(takeoff, date, land).match()
                phase1.append(f)
            phase2 = []
            for l2 in list:
                takeoff = l[1]
                land = l[2]
                date = self.date
                f2 = DirectFlight(takeoff, date, land).match()
                phase2.append(f2)
            combolist.append(phase1)
            combolist.append(phase2)
        if self.connection == 2:
            phase1 = []
            for l in list:
                takeoff = l[0]
                land = l[1]
                date = self.date
                f = DirectFlight(takeoff, date, land).match()
                phase1.append(f)
            phase2 = []
            for l2 in list:
                takeoff = l[1]
                land = l[2]
                date = self.date
                f2 = DirectFlight(takeoff, date, land).match()
                phase2.append(f2)
            phase3 = []
            for l3 in list:
                takeoff = l[2]
                land = l[3]
                date = self.date
                f3 = DirectFlight(takeoff, date, land).match()
                phase3.append(f3)
            combolist.append(phase1)
            combolist.append(phase2)
            combolist.append(phase3)

        return combolist





if __name__ == '__main__':
    b = ['test', 'YVR', 'NYC', '20210520', 2, 12, '600', 'Economy']
    c = Connection(b)
    start = c.takeoff
    des = c.TakeOffFlights(start)
    d = c.PossiblePath()
    f = c.flightCombo(d)


    print(f)









