import sys
import threading

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from threading import *


import WelcomePageUI
import server_side.SignUp
from server_side import SignIn
import RegisterWindow
import HomeWindow


class login_window(QMainWindow, WelcomePageUI.Ui_WelcomPage):
    def __init__(self):
        # 这里需要重载一下Login_window，同时也包含了QtWidgets.QMainWindow的预加载项。
        super(login_window, self).__init__()
        self.setupUi(self)

        # connect button in welcome page
        self.LoginButton.clicked.connect(self.login)
        self.SingUpButton.clicked.connect(self.signup)

    def login(self):
        # read input text (String)
        userName = self.UserNameEdit.text()
        passWord = self.PasswordEdit.text()
        # check info if they are correct
        result = SignIn.SignIn(userName, passWord)
        if result == False:
            print(result)
            QMessageBox.warning(self, "Warning", "Wrong User Info, please try again！")
            # self.close()
            return False
        else:
            # good login, pass user info [('test', '1', '1', '1', 'YVR', '123')]
            self.close()
            self.homeUI = HomeWindow.HomePage(result)
            self.homeUI.show()
            print('cool login')

    def signup(self):
        self.w = RegisterWindow.Register()
        self.w.show()



if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = login_window()
    window.show()
    sys.exit(app.exec_())