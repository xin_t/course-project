# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wrongInfo.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WrongInfo(object):
    def setupUi(self, WrongInfo):
        WrongInfo.setObjectName("WrongInfo")
        WrongInfo.resize(513, 258)
        self.centralwidget = QtWidgets.QWidget(WrongInfo)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(110, 30, 271, 61))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(180, 170, 131, 40))
        self.pushButton.setObjectName("pushButton")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(60, 110, 391, 31))
        self.label_2.setObjectName("label_2")
        WrongInfo.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(WrongInfo)
        self.statusbar.setObjectName("statusbar")
        WrongInfo.setStatusBar(self.statusbar)

        self.retranslateUi(WrongInfo)
        QtCore.QMetaObject.connectSlotsByName(WrongInfo)

    def retranslateUi(self, WrongInfo):
        _translate = QtCore.QCoreApplication.translate
        WrongInfo.setWindowTitle(_translate("WrongInfo", "MainWindow"))
        self.label.setText(_translate("WrongInfo", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Wrong User Info</span></p></body></html>"))
        self.pushButton.setText(_translate("WrongInfo", "OK"))
        self.label_2.setText(_translate("WrongInfo", "<html><head/><body><p><span style=\" font-size:12pt;\">Please try again or sign up</span></p></body></html>"))

