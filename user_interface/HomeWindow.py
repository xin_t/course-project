import sys
import os
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import home
from server_side import Address
from time import sleep
import delAddressWindow

class HomePage(QMainWindow, home.Ui_Home):

    def __init__(self, result):
        super(HomePage, self).__init__()
        self.setupUi(self)
        # personal page info display
        self.restart = result
        self.firstnameLabe.setText('Hello %s' % result[0][1])
        self.emailLabel.setText(result[0][0])
        self.iataEdit.setText(result[0][4])
        self.result = result
        # address page info display
        # self.address_list = Address.show_address(result[0][0])
        # self.addr_list = []
        # for i in range(len(self.address_list)):
        #     info = self.address_list[i]
        #     text_display = '''%s, %s, %s ;''' % (info[0], info[1], info[2])
        #     self.addr_list.append(text_display)
        self.address_display()


    # display address when login success
    def address_display(self):
        address_list = Address.show_address(self.result[0][0])
        addr_list = []
        for i in range(len(address_list)):
            info = address_list[i]
            text_display = '''%s, %s, %s ;''' % (info[0], info[1], info[2])
            addr_list.append(text_display)
        slm = QStringListModel()
        slm.setStringList(addr_list)
        self.adressListView.setModel(slm)


    # delete selected address
    def del_address(self):
        # find index of selected item
        idx = self.adressListView.currentIndex().row()
        del_info = self.address_list[idx]
        update = Address.update_address(del_info[0], del_info[1], del_info[2], del_info[3])
        update.delete()




    # restart from welcome page

















if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = HomePage()
    window.show()
    sys.exit(app.exec_())
