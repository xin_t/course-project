import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import newAddress
from server_side import Update

class newAddress(QMainWindow, newAddress.Ui_MainWindow):
    def __init__(self, email):
        super(newAddress, self).__init__()
        self.setupUi(self)
        self.addButton.clicked.connect(self.add_now)
        self.email = email
        self.userEmail.setText(self.email)

    def add_now(self):
        # get info from window
        street = self.streetText.text()
        state = self.stateText.text()
        country = self.countryText.text()
        # call add function
        user = Update.UpdateUser(self.userEmail.text())
        user.add_address(street, state, country)
        QMessageBox.information(self, 'Success', 'Address added')
        self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = newAddress()
    window.show()
    sys.exit(app.exec_())
