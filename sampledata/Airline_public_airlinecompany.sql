INSERT INTO public.airlinecompany (airlinecode, name, country) VALUES ('AA', 'American Airline', 'US');
INSERT INTO public.airlinecompany (airlinecode, name, country) VALUES ('DL', 'Delta Airline', 'US');
INSERT INTO public.airlinecompany (airlinecode, name, country) VALUES ('WJ', 'West Jet', 'US');
INSERT INTO public.airlinecompany (airlinecode, name, country) VALUES ('AC', 'Air Canada', 'Canada');