# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'WelcomePageUI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from server_side import SignIn
from PyQt5.QtCore import QCoreApplication
from time import sleep


class Ui_WelcomPage(object):
    def setupUi(self, WelcomPage):
        WelcomPage.setObjectName("WelcomPage")
        WelcomPage.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(WelcomPage)
        self.centralwidget.setObjectName("centralwidget")
        self.Welcomelabel = QtWidgets.QLabel(self.centralwidget)
        self.Welcomelabel.setGeometry(QtCore.QRect(310, 90, 561, 71))
        font = QtGui.QFont()
        font.setFamily("Arial Rounded MT Bold")
        font.setPointSize(18)
        self.Welcomelabel.setFont(font)
        self.Welcomelabel.setTextFormat(QtCore.Qt.PlainText)
        self.Welcomelabel.setAlignment(QtCore.Qt.AlignCenter)
        self.Welcomelabel.setObjectName("Welcomelabel")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(200, 250, 801, 452))
        self.widget.setObjectName("widget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(30)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.UserNameEdit = QtWidgets.QLineEdit(self.widget)
        self.UserNameEdit.setEnabled(True)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(14)
        self.UserNameEdit.setFont(font)
        self.UserNameEdit.setText("")
        self.UserNameEdit.setPlaceholderText("")
        self.UserNameEdit.setObjectName("UserNameEdit")
        self.horizontalLayout.addWidget(self.UserNameEdit)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(16)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.PasswordEdit = QtWidgets.QLineEdit(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(14)
        self.PasswordEdit.setFont(font)
        self.PasswordEdit.setPlaceholderText("")
        self.PasswordEdit.setObjectName("PasswordEdit")
        self.horizontalLayout_2.addWidget(self.PasswordEdit)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.line = QtWidgets.QFrame(self.widget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSpacing(30)
        self.verticalLayout.setObjectName("verticalLayout")
        # login button
        self.LoginButton = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(14)
        self.LoginButton.setFont(font)
        self.LoginButton.setObjectName("LoginButton")
        self.verticalLayout.addWidget(self.LoginButton)
        self.SingUpButton = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setFamily("Adobe Devanagari")
        font.setPointSize(14)
        self.SingUpButton.setFont(font)
        self.SingUpButton.setObjectName("SingUpButton")
        self.verticalLayout.addWidget(self.SingUpButton)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        WelcomPage.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(WelcomPage)
        self.statusbar.setObjectName("statusbar")
        WelcomPage.setStatusBar(self.statusbar)

        # link loginButton to function
        # self.LoginButton.clicked.connect(self.login)


        self.retranslateUi(WelcomPage)
        QtCore.QMetaObject.connectSlotsByName(WelcomPage)

    # login function
    def login(self):
        # read input text (String)
        userName = self.UserNameEdit.text()
        passWord = self.PasswordEdit.text()
        # check info if they are correct
        result = SignIn.SignIn(userName, passWord)
        if result == False:
            self.close
            print(result)
            return False
        else:
            print(result)




    def retranslateUi(self, WelcomPage):
        _translate = QtCore.QCoreApplication.translate
        WelcomPage.setWindowTitle(_translate("WelcomPage", "MainWindow"))
        self.Welcomelabel.setText(_translate("WelcomPage", "Welcome to Group 4 Airline"))
        self.label.setText(_translate("WelcomPage", "User Name"))
        self.label_2.setText(_translate("WelcomPage", "Password   "))
        self.LoginButton.setText(_translate("WelcomPage", "Login"))
        self.SingUpButton.setText(_translate("WelcomPage", "Sing Up"))

