from server_side.Database import Data

class UpdateUser(object):
    def __init__(self, email):
        self.connect = Data().connect()
        self.email = email

    # update password
    def password(self, password):
        cursor = self.connect.cursor()
        sql = '''UPDATE customer SET password = '%s' WHERE customeremail = '%s';'''%(password, self.email)
        cursor.execute(sql)
        cursor.execute("""commit;""")
        cursor.close()
        self.connect.close()

    # update iata
    def iata(self, iata):
        # check iata
        iata_valid = Data().check_iata(iata)
        if iata_valid:
            cursor = self.connect.cursor()
            sql = '''UPDATE customer SET iata = '%s' WHERE customeremail = '%s';''' % (iata, self.email)
            cursor.execute(sql)
            cursor.execute("""commit;""")
            cursor.close()
            self.connect.close()
            return True
        else:
            return False

    def add_address(self, street, state, country):
        cursor = self.connect.cursor()
        sql = '''INSERT INTO address VALUES ('%s','%s','%s','%s');''' % (street,state,country, self.email)
        cursor.execute(sql)
        cursor.execute("""commit;""")
        cursor.close()
        self.connect.close()